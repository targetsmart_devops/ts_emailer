from email import message_from_string
from ts_emailer import send_email


def test_send_email(mocker):
    def mock_s3(cls, operation_name, kwarg):
        if operation_name == "SendRawEmail":
            assert kwarg["Source"] == "support@targetsmart.com"
            assert kwarg["Destinations"] == sorted(
                ["to@example.com", "cc@example.com", "bcc@example.com"]
            )
            msg = message_from_string(kwarg["RawMessage"]["Data"])
            assert msg.is_multipart()
            return {"MessageId": 1}

    mocker.patch("botocore.client.BaseClient._make_api_call", new=mock_s3)
    msg_id = send_email(
        "subject",
        "text",
        "html",
        to_addresses=["to@example.com"],
        cc_addresses=["cc@example.com"],
        bcc_addresses=["bcc@example.com"],
        attachments=((__file__, "path.pdf"),),
    )
    assert msg_id == 1
