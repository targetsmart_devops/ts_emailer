#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name="ts_emailer",
    py_modules=["ts_emailer"],
    version="0.0.3",
    description="TargetSmart Emailer",
    author="TargetSmart",
    author_email="devops@targetsmart.com",
    url="http://targetsmart.com/",
    download_url="https://bitbucket.org/targetsmart_devops/ts_emailer",
    license="Copyright TargetSmart. All rights reserved",
    install_requires=["boto3"],
)
