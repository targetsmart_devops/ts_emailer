# ts_emailer


Python helper routine for TargetSmart email notifications

## Installation


Put this in your requirements.txt:

git+https://bitbucket.org/targetsmart_devops/ts_emailer.git@master


## Examples

A simple text email sent from 'support@targetsmart.com':

``` python
send_email(
    'this is the subject',
    'this is a simple text body',
    to_addresses=['bob@example.com', 'alice@example.com'])

```


The same but including a html body:


``` python
send_email(
    'this is the subject',
    'look here',
    '<strong>look here</strong>',
    to_addresses=['bob@example.com', 'alice@example.com'])

```

Cc addresses:

``` python
send_email(
    'this is the subject',
    'look here',
    '<strong>look here</strong>',
    to_addresses=['bob@example.com', 'alice@example.com'],
    cc_addresses=['ted@example.com'])

```


Bcc addresses:

``` python
send_email(
    'this is the subject',
    'look here',
    '<strong>look here</strong>',
    to_addresses=['bob@example.com', 'alice@example.com'],
    cc_addresses=['ted@example.com',],
    bcc_addresses=['mary@example.com,])

```


Attachments should be specified as a sequence of file path, label 2-tuples:


``` python
send_email(
    'this is the subject',
    'this is a simple text body',
    to_addresses=['bob@example.com', 'alice@example.com'],
    attachments=(('some/file.pdf', 'cool report.pdf'), ('other/file.xlsx', 'wow.xlsx')))

```


By default the source is support@targetsmart.com. Override using the
`source` kwarg. To specify one or more reply-to addresses, provide a
sequence using the `reply_to_list` kwarg.
