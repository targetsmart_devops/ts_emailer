from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os

import boto3


def send_email(
    subject,
    text,
    html=None,
    to_addresses=None,
    cc_addresses=None,
    bcc_addresses=None,
    source="support@targetsmart.com",
    attachments=None,
    reply_to_list=None,
    aws_region="us-east-1",
):
    """
    Helper wrapper for ses.send_email

    Returns SES messageid upon success
    """
    region = os.environ.get("AWS_DEFAULT_REGION", aws_region)
    client = boto3.client("ses", region_name=region)

    if html is not None:
        message = MIMEMultipart("alternative")
    else:
        message = MIMEMultipart()

    message["subject"] = subject
    message["from"] = source
    destinations = []

    if to_addresses:
        message["to"] = ", ".join(list(to_addresses))
        destinations += list(to_addresses)

    if cc_addresses:
        message["cc"] = ", ".join(list(cc_addresses))
        destinations += list(cc_addresses)

    if bcc_addresses:
        destinations += list(bcc_addresses)

    if reply_to_list:
        message["reply-to"] = ", ".join(list(reply_to_list))

    if text is not None:
        text_part = MIMEText(text, "plain")
        message.attach(text_part)

    if html is not None:
        html_part = MIMEText(html, "html")
        message.attach(html_part)

    if attachments:
        for fname, label in attachments:
            part = MIMEApplication(open(fname, "rb").read())
            part.add_header("Content-Disposition", "attachment", filename=label)
            message.attach(part)

    if not destinations:
        # client.send_raw_email raises an exception if there are no destinations
        return None

    resp = client.send_raw_email(
        Source=source,
        Destinations=sorted(list(set(destinations))),
        RawMessage={"Data": message.as_string()},
    )
    return resp["MessageId"]
